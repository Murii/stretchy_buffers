#ifndef __STRECHY_BUFFERS_H_
#define __STRECH_BUFFERS_H_

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

#ifndef MIN
#define MIN(x, y) ((x) <= (y) ? (x) : (y))
#endif
#ifndef MAX
#define MAX(x, y) ((x) >= (y) ? (x) : (y))
#endif
#ifndef CLAMP_MAX
#define CLAMP_MAX(x, max) MIN(x, max)
#endif
#ifndef CLAMP_MIN
#define CLAMP_MIN(x, min) MAX(x, min)
#endif
#ifndef IS_POW2
#define IS_POW2(x) (((x) != 0) && ((x) & ((x)-1)) == 0)
#endif
#ifndef ALIGN_DOWN
#define ALIGN_DOWN(n, a) ((n) & ~((a) - 1))
#endif
#ifndef ALIGN_UP
#define ALIGN_UP(n, a) ALIGN_DOWN((n) + (a) - 1, (a))
#endif
#ifndef ALIGN_DOWN_PTR
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uintptr_t)(p), (a)))
#endif
#ifndef ALIGN_UP_PTR
#define ALIGN_UP_PTR(p, a) ((void *)ALIGN_UP((uintptr_t)(p), (a)))
#endif
#ifndef offsetof
#define offsetof(st, m) ((size_t)&(((st *)0)->m))
#endif

typedef struct BufHdr {
    size_t len;
    size_t cap;
    char buf[];
} BufHdr;

#define buf__hdr(b) ((BufHdr *)((char *)(b) - offsetof(BufHdr, buf)))

#define buf_len(b) ((b) ? buf__hdr(b)->len : 0)
#define buf_cap(b) ((b) ? buf__hdr(b)->cap : 0)
#define buf_end(b) ((b) + buf_len(b))
#define buf_sizeof(b) ((b) ? buf_len(b)*sizeof(*b) : 0)

#define buf_free(b) ((b) ? (free(buf__hdr(b)), (b) = NULL) : 0)
#define buf_fit(b, n) ((n) <= buf_cap(b) ? 0 : ((b) = buf__grow((b), (n), sizeof(*(b)))))
#define buf_push(b, ...) (buf_fit((b), 1 + buf_len(b)), (b)[buf__hdr(b)->len++] = (__VA_ARGS__))
#define buf_printf(b, ...) ((b) = buf__printf((b), __VA_ARGS__))
#define buf_clear(b) ((b) ? buf__hdr(b)->len = 0 : 0)

void *buf__grow(const void *buf, size_t new_len, size_t elem_size);
char *buf__printf(char *buf, const char *fmt, ...);
#endif
